#! /usr/bin/env python2
#!/usr/bin/env python2
##
## mangacli.py
##
## Copyright (C) 2011 - Manuel Bovo
##
## This is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
##

########################################################################
########################################################################
########################################################################
########################################################################
########################################################################
########################################################################
########################################################################
########################################################################

_CONFIGFILE = "default.conf"
_CONFIG_SECTION_DEFAULT = "Default"
_CONFIG_SECTION_NETWORK = "Network"
_CONFIG_SECTION_MANGA = "Manga"
_CONFIG_SECTION_DOWNLOADER = "Downloader"
_CONFIG_SECTION_ENGINE = "Engine"

########################################################################
########################################################################
 ######  #######    #     # ####### #######    ####### ######  ### ####### 
 #     # #     #    ##    # #     #    #       #       #     #  #     #    
 #     # #     #    # #   # #     #    #       #       #     #  #     #    
 #     # #     #    #  #  # #     #    #       #####   #     #  #     #    
 #     # #     #    #   # # #     #    #       #       #     #  #     #    
 #     # #     #    #    ## #     #    #       #       #     #  #     #    
 ######  #######    #     # #######    #       ####### ######  ###    #    
                                                                           
    #    ####### ####### ####### ######     ####### #     # ###  #####     
   # #   #          #    #       #     #       #    #     #  #  #     #    
  #   #  #          #    #       #     #       #    #     #  #  #          
 #     # #####      #    #####   ######        #    #######  #   #####     
 ####### #          #    #       #   #         #    #     #  #        #    
 #     # #          #    #       #    #        #    #     #  #  #     #    
 #     # #          #    ####### #     #       #    #     # ###  #####     
                                                                           
 ######  ####### ### #     # #######                                       
 #     # #     #  #  ##    #    #                                          
 #     # #     #  #  # #   #    #                                          
 ######  #     #  #  #  #  #    #                                          
 #       #     #  #  #   # #    #                                          
 #       #     #  #  #    ##    #                                          
 #       ####### ### #     #    #          
########################################################################
########################################################################
import lib.web as web
import re, os, sys, time, threading
from datetime import datetime
import pickle
import ConfigParser

print "++++++++++++++++++++++++++++++++++"
print "++++++++ Manga Downloader ++++++++"
print "++++++++     V. 1.1       ++++++++" 
print "++++++++++++++++++++++++++++++++++"
print ""
try:
    config = ConfigParser.ConfigParser()
    config.readfp(open(_CONFIGFILE))
    config.read
    
    _WEBHOST = config.get(_CONFIG_SECTION_NETWORK,'host')
    _MANGA = config.get(_CONFIG_SECTION_MANGA, 'manganame')
    _V = config.get(_CONFIG_SECTION_MANGA, 'volumes').split(',')
    _OUTDIR = config.get(_CONFIG_SECTION_DOWNLOADER, 'output')
    _DRYRUN = config.getboolean(_CONFIG_SECTION_DOWNLOADER,'dryrun')
    _CLEANUP = config.getboolean(_CONFIG_SECTION_ENGINE,'cleanup_temp')

except Exception as error:
    print "-- ERROR: loading configuration file failed"
    print "         ",error
    exit()

imgarray=[]
_MAXERRORS = 20
_VOL = []
for i in _V:
    _VOL.append(int(i))

#sem = threading.Semaphore(1)
#
#class Page(Thread):
#    __doc__='Implementation of a Downloader Object (multithread) for Manga\n'
#    self._WEBHOST="http://www.mangareader.net"
#    self._WEBDIR=""
#    self._VOL=1
#   
#    def __init__(self,webhost="http://www.mangareader.net",webdir="example",vol=1):
#        self._WEBHOST = webhost
#        self._WEBDIR = webdir
#        self._VOL = vol
#        
#    def run(self):
#        """Execute the method download() spawning a new thread."""
#        imglink = self.download()
#        sem.acquire()
#        imgarray.append(imglink)
#        sem.release()#
#
#    def download(self):
#        url = self._WEBHOST + "/"+_MANGA+"/"+str(vol)+"/"
print "++ Config: "
print "     HOST: ("+_WEBHOST+")"
print "    MANGA: "+_MANGA
print "  VOLUMES: ",_VOL
print "   OUTPUT: "+_OUTDIR
if (_DRYRUN):
    print "   DRYRUN: ",_DRYRUN
print "++ Start" 
print ""

for vol in _VOL:

    errors=0
    imgarray=[]

    print "+++ Volume "+str(vol)
    print "---- Retrieving links"

    #if (os.path.exists("./mangareader_"+str((vol-1))+".temp")):
    #    try:
    #        os.remove("./mangareader_"+str((vol-1))+".temp")
    #    except Exception as error:
    #        print error
    #        print "Failed to remove .temp file, please remove it by hand before try another execution!"

    if (os.path.exists("./mangareader_"+str(vol)+".temp")):
        print "------ Temp found! Resuming..."
        f=open("./mangareader_"+str(vol)+".temp","r")
        imgarray=pickle.load(f)
        f.close()
    else:
        #set up multi page
        currpage = "/"+_MANGA+"/"+str(vol)+"/"
        nextpage = currpage

        numpage = 0
        sys.stdout.write( "------ Pages: " )
        while ( re.match(currpage,nextpage) ):

            # Make the url 	eg: http://www.mangareader.net:80/super-darling/1/ 
            url = _WEBHOST + nextpage
            sys.stdout.write (str(numpage) + " ")
            sys.stdout.flush()
            numpage = numpage + 1 

            # create the object and try to connect
            w = web.Web(url)
            res = w.connect()
            if (res is None and w.lasterror is not None ):
                print "---- Error connecting to " + url + "  " + str(w.lasterror)
                continue

            # Retrieve image link
            strarray = res.search("img.*"+_MANGA+".*jpg")

            if (strarray is None):
                sys.stdout.write( "E! ")
                errors = errors + 1
                if ( errors >= _MAXERRORS):
                    raise Exception("-- CRITICAL: Too many errors encountered. Aborted.")
            else:
                for s in strarray:
                    image = re.split("^.*src.\"",s)
                    imgarray.append( image[1] )

            # Retrieve the next page link
            if (strarray is None):
                sys.stdout.write( "E! ")
                errors = errors + 1
                if ( errors >= _MAXERRORS):
                    raise Exception("-- CRITICAL: Too many errors encountered, Aborted.")
            else:
                strarray = res.search(".*img.*"+_MANGA)
                nextpage=""
                for s in strarray:
                    k = re.split("^.*href=\"",s)
                    k = re.split("\".*",k[1])
                    nextpage = k[0]
    #print "------ Saving temp list."
    try:
        f=open("./mangareader_"+str(vol)+".temp","w")
        pickle.dump(imgarray,f)
        f.close()
    except Exception as error:
        print "-- ERROR: Saving tempo file failed!"
        print "         ",error
        
    print "---- Found " + str(len(imgarray)) + " images."

    if (_DRYRUN == False ):
        print "++++ Downloading: "
        outdir = _OUTDIR+"vol_"+str(vol)+"/"
        try:
            print "mkdir " + _OUTDIR
            os.mkdir(_OUTDIR)
        except Exception as error:        
            print error
        try:
            print "mkdir " + outdir
            os.mkdir(outdir)
        except Exception as error:                
            print error
        
        num=1
     
        for img in imgarray:
            if num < 10:
                cnum="00"
            elif num<100:
                cnum="0"
            cnum = cnum + str(num)
        
            web.download(img,outdir+cnum+".jpg",reporthook=web.myProgressBar,output=True)
            num = num + 1
    else:
        print "---- DRY run! Skip downloading\n"

print ""
if (_CLEANUP == True):        
    # cleaning up temps
    try:
        os.remove("./mangareader_*.temp")
    except Exception as error:
        print "-- ERROR: Failed to cleanup .temp files, please remove it by hand before try another execution!"
        print "        ",error
else:
    print "++ Keeping temp files in place (edit "+_CONFIGFILE+" to change this)"

print "++ Done."
