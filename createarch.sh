#!/bin/bash

echo "+Archiving directories into cbz files"

for i in `find -type d -name 'vol*'`
do
	echo -en "++Archiving [$i] "
	zip $i.zip $i/* 2> /dev/null | while read x; do echo -en "."; done; echo ""
	mv "$i.zip" "$i.cbz" 2> /dev/null > /dev/null
done
