# README #

Manga Downloader is a python script to automatic download (one or more) volumes of your favourite manga from mangareader.net (and more in the future).
This aim to create cbz/cbr files to put it on ebook readers for convenient offline access.

### Prerequisites ###

* Python 2.7.x
* advanced users who can launch scripts from command line

### How it works? ###

There are two version of this tool, one from any shell or command line (CLI)
and one from every desktop envtironment (GUI)

#### CLI ####

* edit default.conf to your needings
* `python2 mangacli.py`
* Wait...

#### GUI ####

* edit default.conf to you needings
* `python2 mangagui.py`
* use the gui


#### To create cbz files ####

* `cd ./outputdir` (the one you have setupped before)
* `bash createarch.sh`
* CBZ files are ready to use.
